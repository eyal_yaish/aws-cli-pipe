#!/usr/bin/env bash

set -e

# required parameters
AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID:?'AWS_ACCESS_KEY_ID environment variable missing.'}
AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY:?'AWS_SECRET_ACCESS_KEY environment variable missing.'}
AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION:?'AWS_DEFAULT_REGION environment variable missing.'}
ENV=${ENV:?'ENV environment variable missing.'}

enable_debug() {
  if [[ "${DEBUG}" == "true" ]]; then
    info "Enabling debug mode."
    set -x
  fi
}
enable_debug


KEYS=`(aws --region eu-west-1 secretsmanager get-secret-value --secret-id aws_keys --query "SecretString" | jq -r '.' | jq -r --arg ENV "$BITBUCKET_BRANCH" '.[$ENV]')`
AWS_ACCESS_KEY_ID=`(echo $KEYS | jq -r '.AWS_ACCESS_KEY_ID')`
AWS_SECRET_ACCESS_KEY=`(echo $KEYS | jq -r '.AWS_SECRET_ACCESS_KEY')`
AWS_DEFAULT_REGION=`(echo $KEYS | jq -r '.AWS_DEFAULT_REGION')`
ECR_ADDRESS=`(echo $KEYS | jq -r '.ECR_ADDRESS')`


echo "#!/bin/bash" > $BITBUCKET_PIPE_STORAGE_DIR/env.sh
chmod +x $BITBUCKET_PIPE_STORAGE_DIR/env.sh
echo AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID >> $BITBUCKET_PIPE_STORAGE_DIR/env.sh
echo AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY >> $BITBUCKET_PIPE_STORAGE_DIR/env.sh
echo AWS_DEFAULT_REGION=$AWS_DEFAULT_REGION >> $BITBUCKET_PIPE_STORAGE_DIR/env.sh
echo ECR_ADDRESS=$ECR_ADDRESS >> $BITBUCKET_PIPE_STORAGE_DIR/env.sh

if [[ "${status}" == "0" ]]; then
  success "Success!"
else
  fail "Error!"
fi